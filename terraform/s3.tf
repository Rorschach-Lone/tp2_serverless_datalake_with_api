# TODO : Create a s3 bucket with aws_s3_bucket

resource "aws_s3_bucket" "here_is_a_super_s3_bucket" {
  bucket = "here-is-a-super-s3-bucket"
  acl    = "public-read"
  force_destroy = true
}

# TODO : Create 1 nested folder :  job_offers/raw/  |  with  aws_s3_object

resource "aws_s3_bucket_object" "object" {
  bucket = aws_s3_bucket.here_is_a_super_s3_bucket.id
  key    = "job_offers/raw/"
  source = "/dev/null"
}

# TODO : Create an event to trigger the lambda when a file is uploaded into s3 with aws_s3_bucket_notification